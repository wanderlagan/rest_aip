 let verify_login = function () {
    if (localStorage.admin === undefined) {
        location.href = '../../login.html'
    }

    let admin = JSON.parse(localStorage.admin);
    $("[user_login]").html(`${admin.admin_name} ${admin.admin_lastname}`);
}
verify_login();