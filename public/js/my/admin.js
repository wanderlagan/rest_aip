let Admin = {
    reg: function (_data) {
        $.ajax({
            url :"/reg_admin",
            method: "POST",
            processData: false,
            contentType: false,
            mimeType: "multipart/form-data",
            dataType: "json",
            data : _data,
            success(e) {
                if( e.result ) {
                    console.log(e);
                }else console.log(e)
            }
        });
    }
};

$("[reg_admin]").on("click", function () {
    let formdata = new FormData();
    if ( $("[photo]")[0].files.length > 0 ) {
        formdata.append("attaches", ($("[photo]")[0].files[0]));
    }
    formdata.append("data", JSON.stringify({
        name: $("[first_name]").val(),
        lastname: $("[last_name]").val(),
        email: $("[email]").val(),
        password: $("[password]").val(),
        foto: null
    }));
    Admin.reg( formdata );
})