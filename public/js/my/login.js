let user = {
    do_login: function (_data) {
        $.ajax({
            url :"/login",
            method: "POST",
            contentType: "application/json",
            data : JSON.stringify({_data}),
            success(e) {
               if( e.result ) {
                   localStorage.admin = JSON.stringify( e.data )
                   location.href = '../../index.html';
                }else alert("email ou senha errada!")
            }
        });
    },
    log_out: function () {
        localStorage.removeItem('admin');
        verify_login();
    }
}

$("[logout_user]").on("click", function () {
    user.log_out();
});

$("[btn_login]").on("click", function () {
    let _email = $("#inputEmail").val()
    let _password = $("#inputPassword").val()
    if ( _email.trim() === '' || _password.trim() === ''  ) {
        alert("Campos Vazios!");
        return;
    }

    let _data = {
        email: _email,
        password: _password
    }

    user.do_login(_data);
});