const postsData = require('../database_router/posts.database.router');

exports.regAdmin = function (data) {
    return postsData.regAdmin(data);
};
exports.login = function (data) {
    return postsData.login(data);
};

exports.getPostsById = async function (id) {
    return postsData.getPostsById(id);
};

exports.savePost = function (post) {
    return postsData.savePost(post);
};

exports.deletePost = function (id) {
    return postsData.deletePost(id);
};

exports.updatePost = function (post) {
    return postsData.updatePost(post);
};