// este script rodo servidor na porta 3000
var cons = require('consolidate');
const express = require('express');
const upload = require('express-fileupload');
const path = require("path");
const app = express();
const port = 3000
const hostname = "127.0.0.1"

app.use(express.static(path.join(__dirname, '../public')))
app.use( upload() );
app.use( express.json() );
app.engine('html', cons.swig)
app.set('views', path.join(__dirname, '../public'));
app.set('view engine', 'html');

// usar as rotas dentro da instancia do express

app.use('/', require('./route/all.route'));

app.listen(port, hostname,() =>{
    console.log("Rodando o servidor em http://127.0.0.1:3000/");
})