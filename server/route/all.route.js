const express = require('express');
const app = express.Router();
const postService =  require('../service/posts.service');

app.get('/', function (req, res) {
    res.render('index')
});

app.post('/login', async function (req, res, next) {
    let log = req.body._data
    const response =  await postService.login( log );
    res.json({
        result: response.funct_login.result,
        data: response.funct_login.data
    });

});

app.post('/reg_admin', async function(req, res){
    let admin = JSON.parse(req.body.data)
    let filename = null;
    if ( req.files !== null ) {
        filename = req.files.attaches.name;
        var _files = req.files.attaches
        _files.mv('./public/img/'+filename, function (err) {
            if (err){
                console.log(`Erro ao enviar a imagem`+ err)
            }else{
                console.log("Imagem enviada")
            }
        });
    }
    admin.foto = filename;
    let new_data = JSON.stringify(admin)

    const response = await postService.regAdmin( new_data );
    res.json({
        result: response.funct_reg_admin.result,
        message: response.funct_reg_admin.message
    });
});

app.post('/getById', async function(req,res){
    const post = req.body;
    const response =  await postService.getPostsById(post.post_id );
    res.json( response );
});

app.post('/save_post', async  function(req,res){
    const post = req.body;
    const response = await postService.savePost(post)
    res.status(201).json( response )
});

app.put('/update_post', async function(req, res){
    const post = req.body;
    await postService.updatePost(post)
    res.status(204).end()
});

app.delete('/delete_post', async function(req,res){
    const post = req.body;
    await postService.deletePost(post.post_id )
    res.status(204).end()
});

module.exports = app;