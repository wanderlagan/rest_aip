const pgp = require('pg-promise')();

const db_conn = pgp({
    user: 'rest_api_sa',
    password: '1234',
    host: 'localhost',
    port: 5432,
    database: 'rest_api_db',
});

module.exports = db_conn;