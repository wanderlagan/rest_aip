const axios = require('axios');
const crypto = require('crypto');
const postService =  require('../service/posts.service');

// esta função serve para gerar texto aleatorios para o teste
const generate_text = function () {
    return crypto.randomBytes(20).toString('hex');
};

const request = function (url, method, data) {
  return axios({
        url: url,
        method: method,
        data: data,
        validateStatus: false
  })
};

test('Should get post', async function(){
   const post_1 = await postService.savePost({
        title: generate_text(),
        content:generate_text()
   });
   const post_2 = await postService.savePost({
        title: generate_text(),
        content:generate_text()
   });
   const post_3 = await postService.savePost({
        title: generate_text(),
        content:generate_text()
   });

    const response = await request('http://localhost:3000/posts','get',);
    const posts = response.data;

    expect(response.status).toBe(200);
    expect(posts).toHaveLength(3);

    await postService.deletePost(post_1.post_id);
    await postService.deletePost(post_2.post_id);
    await postService.deletePost(post_3.post_id);
});

test('Should save post', async function(){
    // given - dado que
    const _data = {
        title: generate_text(),
        content:generate_text()
    }

    const response = await request('http://localhost:3000/save_post','post', _data);
    const post = response.data;
    expect(response.status).toBe(201) // created
    expect(post.post_title).toBe(_data.title);
    expect(post.post_content).toBe(_data.content)
    await postService.deletePost(post.post_id);
});

test('Should update post', async function(){
    // given - dado que
    const _data = await postService.savePost({
        title: generate_text(),
        content:generate_text()
    });

    _data.post_title = generate_text();
    _data.post_content = generate_text();

   const response = await request(`http://localhost:3000/update_post`,'put', _data);
   expect(response.status).toBe(204)
   const updated = await request(`http://localhost:3000/getById`,'post', _data);
   const updatedPost = updated.data
   expect(updatedPost.post_title).toBe(_data.post_title);
   expect(updatedPost.post_content).toBe(_data.post_content)
   await postService.deletePost(updatedPost.post_id);
});

test('Should delete post', async function(){
    const _data = await postService.savePost({
        title: generate_text(),
        content:generate_text()
    });

   const response = await request(`http://localhost:3000/delete_post`,'delete', _data);
   expect(response.status).toBe(204)
   const posts = await postService.getPosts()
   expect(posts).toHaveLength(0)
});