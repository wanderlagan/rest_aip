const database = require('../db_connection/database');

exports.login = function (data) {    return database.one('select * from blog.funct_login($1)', [data]);
}
exports.regAdmin = function (data) {
    return database.one('select * from blog.funct_reg_admin($1)', [data]);
};

exports.getPostsById = function (id) {
    return database.one('select * from blog.tbl_post where post_id = $1', [id]);
};

exports.savePost = function (post) {
    return database.one('insert into blog.tbl_post (post_title, post_content) values ( $1, $2) returning *', [post.title, post.content])
};

exports.updatePost = function (post) {
    return database.none('update blog.tbl_post set post_title = $1, post_content = $2 where post_id = $3',[post.post_title, post.post_content, post.post_id])
};

exports.deletePost = function (id) {
    return database.none('delete from blog.tbl_post where post_id = $1',[id])
};